package personenliste;

public class Lebewesen implements Comparable{
	int mAlter;
	String mGeschlecht;
	
	public Lebewesen(String alter, String geschlecht){
		this.mAlter = Integer.parseInt(alter);
		this.mGeschlecht = geschlecht;
	}

	@Override
	public int compareTo(Object o) {
		
			Lebewesen other = (Lebewesen) o;

			//a negative integer, zero, or a positive integer
			//as this object is less than, equal to, or greater than the specified object.
			if(this.mAlter > other.mAlter) return 1 ;
			else if(this.mAlter == other.mAlter) return 0;
			else return -1;
		
		
	}

	public int getmAlter() {
		return mAlter;
	}

	public void setmAlter(int mAlter) {
		this.mAlter = mAlter;
	}

	public String getmGeschlecht() {
		return mGeschlecht;
	}

	public void setmGeschlecht(String mGeschlecht) {
		this.mGeschlecht = mGeschlecht;
	}
	
}
