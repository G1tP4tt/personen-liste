package personenliste;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Lebewesenliste {

	
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		InputStreamReader isr = new InputStreamReader(Lebewesenliste.class.getResourceAsStream("personendaten.txt"));
		final String regex = "[;]";
		ArrayList<Lebewesen> lebewesen = new ArrayList<Lebewesen>();

		BufferedReader br = new BufferedReader(isr);

		String zeile;
		try {
			while ((zeile = br.readLine()) != null) {
				String purifiedString = zeile.replace("\"", "");
				purifiedString = purifiedString.replace(" ", "");
				String lebewesendaten[] = purifiedString.split(regex);
				if(lebewesendaten.length==4){
					Person person = new Person(lebewesendaten[0], lebewesendaten[1], lebewesendaten[2], lebewesendaten[3]);
					lebewesen.add(person);
				}else{
					Tier tier = new Tier(lebewesendaten[0], lebewesendaten[1], lebewesendaten[2]);
					lebewesen.add(tier);
				}
				
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Collections.sort(lebewesen);
		for(Lebewesen l: lebewesen){
			
			System.out.println(l.toString()+ "");
		}
	}
}
