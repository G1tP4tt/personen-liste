package personenliste;

public class Person extends Lebewesen implements Comparable {
	String mName;
	String mVorname;

	public Person(String geschlecht, String alter, String vorname, String name) {
		super(alter, geschlecht);
		this.mName = name;
		this.mVorname = vorname;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(Object o) {
		if(o.getClass() == this.getClass()){
			Person other = (Person) o;
			return this.mName.compareTo(other.mName);
		}
		return 0;
	}

	@Override
	public String toString() {
		return "Person [mName=" + mName + ", mVorname=" + mVorname + ", mAlter=" + mAlter + ", mGeschlecht="
				+ mGeschlecht + "]";
	}
	
	
	
	

}
