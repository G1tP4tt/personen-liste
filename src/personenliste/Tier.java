package personenliste;

public class Tier extends Lebewesen {

	String mId;
	public Tier(String geschlecht, String alter, String id) {
		super(alter, geschlecht);
		this.mId = id ;
	}
	@Override
	public String toString() {
		return "Tier [mId=" + mId + ", mAlter=" + mAlter + ", mGeschlecht=" + mGeschlecht + "]";
	}
	
	

}
